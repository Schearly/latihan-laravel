@extends('layout.master')

@section('judul')
Halaman Detail Kategori {{$kategori->nama}}
@endsection

@section('content')
<h3>{{$kategori->nama}}</h3>
<p>{{$kategori->deskripsi}}</p>
@endsection