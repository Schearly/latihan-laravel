<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        $nama1 = $request->nama_depan;
        $nama2 = $request->nama_belakang;

        return view('halaman.welcome', compact('nama1', 'nama2'));
    }
}
