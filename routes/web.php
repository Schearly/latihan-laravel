<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');
Route::get('/register', 'AuthController@regis');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-table', function(){
    return view('table.data-table');
});

//CRUD Kategori
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');